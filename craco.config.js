const CopyPlugin = require('copy-webpack-plugin'); //version 6.3.2, new version of copy-webpack-plugin won't work with craco.
const path = require('path');

module.exports = {
  devServer: {
    writeToDisk: true
  },
  webpack: {
    plugins: [
      new CopyPlugin({
        patterns: [
          {
            context: path.resolve(__dirname, "src"),
            from: "tinymceContentStyle.css",
            to: path.resolve(__dirname, "public", "tinymceContentStyle.css"),
            force: true,
          },
        ],
      }),
    ]
  },
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
}