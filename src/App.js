import { useState } from 'react'

import AnswerEditor from './components/editors/AnswerEditor';
import AnswerPanel from './components/panels/AnswerPanel';
import QuestionPanel from './components/panels/QuestionPanel';
import QuestionSection from './components/QuestionSection';
import { answers, getMockedDate, question, thisUser } from './mockData.js'
import useNoOutlineWhenUsingMouse from './hooks/useNoOutlineWhenUsingMouse';
import Background from './components/Background';
import Logo from './components/Logo';


function App() {
  useNoOutlineWhenUsingMouse();

  const [inputText, setInputText] = useState("");
  const [inputHtml, setInputHtml] = useState("");
  const [userAnswer, setUserAnswer] = useState({ user: thisUser, content: "" });
  const [questionMinimized, setQuestionMinimized] = useState(true);

  const userHasSubmitted = userAnswer.content.length > 0;

  const handleEditorChange = (value, editor) => {
    setInputHtml(value);
    setInputText(editor.getContent({ format: 'text' }))
  };

  const answerPanels = answers.map(a => (
    <div className="mt-4">
      <AnswerPanel answer={a} />
    </div>
  ));

  const questionPanel = (
    <QuestionPanel question={question} />
  );

  const handleSubmit = () => {
    //elements have empty innerText and have no images or videos
    if (
      (inputText.length === 0 && !/img|iframe/.test(inputText))
      || /^[\n\s]+$/.test(inputText)
    )
      return;

    if (inputText.length > 30000) {
      //TODO: show message: text limit exceeded.
      return;
    }

    setUserAnswer({
      date: getMockedDate(),
      user: thisUser,
      rated: false,
      rating: 0,
      content: inputHtml,
    });

    //TODO: Answer submission Backend stuffs
  };

  return (
    <>
      <div className="z-20 fixed w-full">
        <QuestionSection
          minimized={questionMinimized}
          questionPanel={questionPanel}
        />
      </div>

      <div className="relative overflow-x-hidden flex bg-primary" >
        {/* TODO: sidebar */}
        {/* <div className="w-64">4LEAF</div> */}
        <Background />
        <div className="relative z-20">
          <Logo />
        </div>
        <div className="mt-24 relative mx-auto max-w-sm sm:max-w-lg md:max-w-2xl lg:max-w-3xl xl:max-w-4xl">
          <div className="flex flex-col min-h-screen">
            <div className="relative">
              {questionPanel}
            </div>

            <div className="pt-2 bg-secondary shadow-md">
              {userHasSubmitted > 0 &&
                <div className="mt-4">
                  <AnswerPanel answer={userAnswer} />
                </div>
              }

              {answerPanels.length > 0 &&
                answerPanels
              }
            </div>

            <div className="h-36"></div>
          </div>
        </div>
      </div>

      <div className="z-20 fixed bottom-0 inset-x-0 mx-auto flex justify-center">
        <AnswerEditor
          onEditorChange={handleEditorChange}
          onClickShowQuestion={() => setQuestionMinimized(minimized => !minimized)}
          onClickSubmit={handleSubmit}
          submitted={userHasSubmitted}
        />
      </div>
    </>
  );
}

export default App;

