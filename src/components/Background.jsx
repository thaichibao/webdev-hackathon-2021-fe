const Background = () => {
  return (
    <>
      <svg className="absolute mt-52" width="100%" viewBox="0 0 1440 592" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1439.5 592L-6.5 430V244.5L1439.5 0.5V592Z" fill="#89bcc9" />
      </svg>

      <svg className="fixed z-10 pointer-events-none transform -translate-y-8 sm:-translate-y-4 lg:translate-y-0" height="920" viewBox="0 0 1159 687" >
        <path d="M-19 0.500061H1473V53.7949H727H-19V0.500061Z" fill="#273f5c" />
      </svg>

      <svg className="fixed z-0 pointer-events-none transform -translate-y-8 sm:-translate-y-4 lg:translate-y-0" height="920" viewBox="0 0 1159 687" >
        <path d="M-26.946 13.5305L1464.96 30.0754L1465.34 73.0586L719.384 64.7861L-26.5697 56.5136L-26.946 13.5305Z" fill="#89bcc9" />
      </svg>
    </>
  )
}
export default Background;