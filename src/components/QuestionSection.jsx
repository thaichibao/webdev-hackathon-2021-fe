import { useRef, useEffect } from 'react'
import anime from 'animejs';

const MINIMIZED_SECTION_HEIGHT = 0;

const QuestionSection = ({ questionPanel, minimized }) => {
  const questionSectionRef = useRef();

  useEffect(() => {
    questionSectionRef.current.style.height = MINIMIZED_SECTION_HEIGHT;
  }, []);

  useEffect(() => {
    const newHeight = minimized ? MINIMIZED_SECTION_HEIGHT : window.innerHeight;
    animateSectionHeight(newHeight);
  }, [minimized]);

  const animateSectionHeight = (height) => {
    anime({
      targets: questionSectionRef.current,
      height: height,
      easing: "easeOutCubic",
      duration: 300,
    })
  };

  return (
    <>
      <div
        ref={questionSectionRef}
        className="bg-quaternary-dark overflow-auto no-scrollbar"
      >
        <div className="max-w-4xl mx-auto mt-24">
          {questionPanel}
        </div>

        <div className="h-36"></div>
      </div>
    </>
  )
}
export default QuestionSection;