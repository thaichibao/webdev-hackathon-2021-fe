import anime from "animejs";
import { useEffect, useRef, useState } from "react";
import { STAR_COLOR } from "../config";
import irandomRange from "../irandomRange";

const Rating = ({ rated: ratedInitial, rating: ratingInitial }) => {
  const [rated, setRated] = useState(ratedInitial);
  const [rating, setRateCount] = useState(ratingInitial);
  const starRef = useRef();

  const handleClick = () => {
    setRateCount(rc => rated ? (rc - 1) : (rc + 1));
    setRated(rated => !rated);
    //TODO: RATING: BACKEND STUFF
  };

  useEffect(() => {
    anime({
      targets: starRef.current,
      scale: [1.3, 1],
      rotate: [irandomRange(0, 10), 0],
      easing: 'spring(1, 100, 10, 10)',
    });
  }, [rated]);

  return (
    <button className="flex items-center"
      onClick={handleClick}
    >
      <svg ref={starRef} className="inline" width="25" height="25" viewBox="0 0 84 80" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M39.326 2.25721C40.4382 0.0706447 43.5618 0.0706418 44.674 2.25721L55.3674 23.281C55.8034 24.1383 56.6241 24.7346 57.5742 24.8844L80.8734 28.5577C83.2967 28.9397 84.2619 31.9105 82.5261 33.6439L65.8357 50.3106C65.1551 50.9902 64.8416 51.955 64.9927 52.9048L68.6991 76.1989C69.0846 78.6216 66.5575 80.4576 64.3725 79.3424L43.3639 68.6191C42.5072 68.1819 41.4928 68.1819 40.6361 68.6191L19.6275 79.3424C17.4425 80.4576 14.9154 78.6216 15.3009 76.1989L19.0073 52.9048C19.1584 51.955 18.8449 50.9902 18.1643 50.3106L1.47394 33.6439C-0.261932 31.9105 0.703327 28.9397 3.12655 28.5577L26.4258 24.8844C27.3759 24.7346 28.1966 24.1383 28.6326 23.281L39.326 2.25721Z"
          fill={rated ? STAR_COLOR : "none"} stroke-width="7" stroke={rated ? "none" : STAR_COLOR} />
      </svg>
      <span className="ml-1 text-lg font-bold text-gray-500">
        {rating}
      </span>
    </button>
  );
}
export default Rating;