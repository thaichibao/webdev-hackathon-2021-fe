const SmallProfile = ({ user }) => {
  //TODO:href: waiting for backend implementation
  return (
    <div>
      <a className="text-quaternary-dark mr-2" href="./#"
      >{user.name}</a>
      <a href="./#">
        <img
          className="rounded-full h-10 w-10"
          src={user.avatar}
          alt="avatar of a user"
        />
      </a>
    </div>
  )
}
export default SmallProfile;