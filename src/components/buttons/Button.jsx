import React from "react";

const Button = ({ text, onClick, extendedClassName = "" }) => {
  return (
    <button
      className={`sm:p-2 rounded-sm font-bold text-white bg-quinary-normal 
       hover:bg-quinary-dark active:bg-white active:text-quinary-dark 
       active:border active:border-quinary-dark
       ${extendedClassName}`}
      onClick={onClick}
    >
      {text}
    </button>
  )
}
export default Button;
