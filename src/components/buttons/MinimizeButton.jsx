import anime from 'animejs';
import React, { useEffect, useRef, useState } from 'react';
import minimizeIcon from '../../assets/minimize-icon.svg'

const MinimizeButton = React.forwardRef(({ onClick, extendedClassName }, ref) => {
  const imgRef = useRef();
  const [minimized, setMinimized] = useState(false);

  useEffect(() => {
    anime({
      targets: imgRef.current,
      rotate: minimized ? "180deg" : "0deg",
      easing: 'easeOutElastic(1.2, .9)',
    });
  }, [minimized]);

  return (
    <button ref={ref} className={`flex justify-center items-center py-1 text-center bg-white border-gray-300 
    hover:bg-gray-100 active:bg-gray-200 ${extendedClassName}`}
      onClick={() => {
        onClick();
        setMinimized(m => !m);
      }}
    >
      <img ref={imgRef} className="h-3" src={minimizeIcon} alt="minimize button"></img>
    </button>
  )
});
export default MinimizeButton;