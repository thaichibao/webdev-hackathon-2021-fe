import { useState } from "react";


const ShowQuestionButton = ({ onClick }) => {
  const [text, setText] = useState("Xem câu hỏi");
  const t1 = "Xem câu hỏi";
  const t2 = "Đóng câu hỏi";

  const handleClick = () => {
    onClick();
    setText(text => text === t1 ? t2 : t1);
  };

  return (
    <button
      className={"text-quaternary bg-white border-gray-300 w-10 sm:w-20 sm:p-2 border-l border-t " +
        "border-b text-md sm:text-xl font-bold hover:bg-gray-100 active:text-white active:bg-quaternary-dark"}
      onClick={handleClick}
    >
      {text}
    </button>
  )
}
export default ShowQuestionButton;