import { useEffect, useRef, useState } from 'react';
import anime from 'animejs';
import MinimizeButton from '../buttons/MinimizeButton';
import CustomEditor from './CustomEditor';

import Button from '../buttons/Button';
import ShowQuestionButton from '../buttons/ShowQuestionButton';
import { MAXIMIZED_EDITOR_HEIGHT, MINIMIZED_EDITOR_HEIGHT } from '../../config';


const AnswerEditor = ({ text, onEditorChange, onClickShowQuestion, onClickSubmit, submitted }) => {
  const [height, setHeight] = useState(MINIMIZED_EDITOR_HEIGHT);
  const [editorIsReady, setEditorIsReady] = useState(false);
  const sectionRef = useRef();
  const editorRef = useRef(null);
  const minimizeButtonRef = useRef();

  const animateSectionHeight = (value, duration = 200) => {
    const editorContainerElement = sectionRef.current;
    const editorElement = editorRef.current.elementRef.current.nextElementSibling;
    const buttonHeight = parseFloat(getComputedStyle(minimizeButtonRef.current).height);
    anime({
      targets: editorElement,
      height: value,
      easing: "easeInOutQuad",
      duration: duration,
    });

    anime({
      targets: editorContainerElement,
      height: value + buttonHeight,
      easing: "easeInOutQuad",
      duration: duration,
    });
  };

  const toggleMinimization = () => {
    if (!editorIsReady) //if editor is not loaded yet, do nothing
      return;

    const minimized = (height === MINIMIZED_EDITOR_HEIGHT);
    const newHeight = minimized ? MAXIMIZED_EDITOR_HEIGHT : MINIMIZED_EDITOR_HEIGHT;

    setHeight(newHeight);
    animateSectionHeight(newHeight);
  }

  useEffect(() => {
    sectionRef.current.style.height = "0px";
  }, []);

  const handleReady = () => {
    const editorElement = editorRef.current.elementRef.current.nextElementSibling;
    editorElement.className += " w-52 sm:w-auto"
    setEditorIsReady(true);
    setTimeout(() => animateSectionHeight(window.innerHeight / 8, 500), 0); //avoid editor width not render correctly
  };

  return (
    <>
      <div ref={sectionRef} className="flex shadow-md">
        <ShowQuestionButton
          onClick={onClickShowQuestion}
        />
        <div className="bg-white flex flex-col overflow-hidden">
          <MinimizeButton
            ref={minimizeButtonRef}
            extendedClassName="border-t border-l border-r"
            onClick={toggleMinimization}
          />
          <div className="flex-grow">
            <CustomEditor
              editorRef={editorRef}
              text={text}
              onEditorChange={onEditorChange}
              onPostRender={handleReady}
            />
          </div>
        </div>
        <Button
          extendedClassName="w-10 text-md sm:w-20 sm:text-2xl"
          text={submitted > 0 ? "Sửa" : "Gửi"}
          onClick={onClickSubmit}
        />
      </div>
    </>

  );
}
export default AnswerEditor;