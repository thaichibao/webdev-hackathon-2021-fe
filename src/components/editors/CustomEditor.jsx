import { Editor } from '@tinymce/tinymce-react'
import { MINIMIZED_EDITOR_HEIGHT } from '../../config.js';

const CustomEditor = ({ editorRef, text, onEditorChange, onPostRender }) => {
  return (
    <Editor
      ref={editorRef}
      value={text}
      init={{
        content_css: 'tinymceContentStyle.css',
        placeholder: "Câu trả lời của bạn...",
        height: MINIMIZED_EDITOR_HEIGHT, //this is the initial height, later changes in this prop will not affect the editor's height
        menubar: false,
        resize: false,
        plugins: [
          'quickbars link media table lists paste',
        ],
        paste_data_images: true,
        quickbars_insert_toolbar: '',
        toolbar_location: 'bottom',
        statusbar: false,
        toolbar:
          'undo redo | formatselect fontsizeselect | bold italic underline | link quickimage media | ' +
          'alignleft aligncenter alignright | bullist numlist | outdent indent | table | help',
      }}
      onEditorChange={onEditorChange}
      onPostRender={onPostRender}

    />
  )
}
export default CustomEditor;