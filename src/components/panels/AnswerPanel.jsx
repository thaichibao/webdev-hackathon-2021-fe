
import ReactHtmlParser from 'react-html-parser';
import Rating from '../Rating';
import SmallProfile from '../SmallProfile';
const AnswerPanel = ({ answer }) => {
  return (
    <div className="border-l-3 p-4 rounded-sm flow-root bg-secondary border-quinary-normal shadow-md">
      <div className="flex justify-between">
        <Rating rated={answer.rated} rating={answer.rating} />
        <SmallProfile user={answer.user} />
      </div>
      <p className="text-sm text-gray-600">
        {`Trả lời hồi ${answer.date}`}
      </p>
      <div className="mt-2 mb-3 border-b border-gray-300"></div>
      <div className="break-words ml-2 mb-4">
        {ReactHtmlParser(answer.content)}
      </div>
    </div>
  )
}
export default AnswerPanel;