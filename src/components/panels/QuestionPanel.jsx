import ReactHtmlParser from 'react-html-parser';
import Rating from '../Rating';
import SmallProfile from '../SmallProfile';

const QuestionPanel = ({ question }) => {

  return (
    <div className="border-t-8 p-4 rounded-sm flow-root bg-secondary border-quaternary-normal shadow-xl">
      <div className="flex justify-between">
        <h3>{question.title}</h3>
        <SmallProfile user={question.user} />
      </div>
      <div className="flex justify-between items-baseline">
        <Rating rated={question.rated} rating={question.rating} />
        <p className="text-sm text-gray-600">
          {`Hỏi hồi ${question.date}`}
        </p>
      </div>
      <div className="mt-2 mb-3 border-b border-gray-300"></div>
      <div className="break-words ml-2">
        {ReactHtmlParser(question.content)}
      </div>
    </div>
  )
}
export default QuestionPanel;