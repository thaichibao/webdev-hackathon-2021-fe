
export const MINIMIZED_EDITOR_HEIGHT = window.innerHeight / 8;
export const MAXIMIZED_EDITOR_HEIGHT = window.innerHeight / 1.5;

export const STAR_COLOR = "#e6e687";
export const LOGO_COLOR_PRIMARY = "#6ED698";
export const LOGO_COLOR_TEXT = "#469465";