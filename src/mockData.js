let mockDate = null;
export function getMockedDate(date) {
  if (mockDate)
    return mockDate;

  const str = new Date().toISOString();
  const [, year, month, day] = str.match(/^(\d{4})-(\d{2})-(\d{2})/);
  mockDate = `${day}/${month}/${year}`;
  return mockDate;
}

export const question = {
  id: "42",
  title: "1 + 1 bằng mấy?",
  content: "<p>Nếu m&igrave;nh nhớ hồi lớp 1 th&igrave;:</p>\n<ul>\n<li>1 + 0 = 1</li>\n<li>1 + 2 = 3</li>\n<li>1 + 3 = 4</li>\n</ul>\n<p>H&igrave;nh như vậy, 1 + 1 sử dụng <strong>ph&eacute;p cộng hai số</strong>.</p>\n<p>Vậy 1 cộng 1 bằng mấy?</p>\n<p>&nbsp;</p>\n<p>C&aacute;c bạn cho m&igrave;nh xin kết quả v&agrave; chứng minh gi&uacute;p m&igrave;nh với.</p><p>&nbsp;</p><p><iframe src=\"https://www.youtube.com/embed/PCu_BNNI5x4\" width=\"560\" height=\"314\" allowfullscreen=\"allowfullscreen\"></iframe></p>",
  date: "1/1/2011",
  rating: 9,
  rated: false,
  user: {
    name: "CMB",
    avatar: "./mock/user1.jpg",
    id: "19521256",
  },
};

export const thisUser = {
  name: "4Leaf",
  avatar: "./mock/user6.jpg",
  id: "4",
};

export const answers = [
  {
    content: "<h5>C&acirc;u trả lời l&agrave; 2.</h5>\n<p>&nbsp;</p>\n<p>Theo một hướng rất căn bản th&igrave; k&yacute; hiệu \"2\" chỉ l&agrave; một c&aacute;ch viết ngắn gọn của 1 + 1 m&agrave; th&ocirc;i, kh&ocirc;ng c&oacute; g&igrave; để m&agrave; chứng minh ở đ&acirc;y cả.</p>\n<p>Nếu m&agrave; ta muốn chứng minh th&igrave; ta cần phải c&oacute; hệ tiền đề để m&agrave; suy ra kết quả m&agrave; ta muốn chứng minh. Ta h&atilde;y bắt đầu với việc sử dụng hệ tiền đề \"th&ocirc;ng thường\" của số tự nhi&ecirc;n, đ&oacute; l&agrave; <a href=\"https://vi.wikipedia.org/wiki/H%E1%BB%87_ti%C3%AAn_%C4%91%E1%BB%81_Peano\">hệ tiền đề Peano</a>. Những tiền đề n&agrave;y cho ta những quy tắc rất cơ bản d&ugrave;ng để m&ocirc; tả số tự nhi&ecirc;n, v&agrave; từ đ&oacute; ta sẽ chứng minh 1 + 1 = 2.</p>\n<p>&nbsp;</p>\n<p>Trong hệ tiền đề n&agrave;y th&igrave; những số như 1 v&agrave; 2 kh&ocirc;ng tồn tại. Ch&uacute;ng ta c&oacute; 0 v&agrave; ch&uacute;ng ta c&oacute; S(n), c&oacute; thể hiểu S(n) như l&agrave; một \"h&agrave;m nối ng&ocirc;i\" (successor function) trong đ&oacute; n&oacute; sản xuất ra con số kế tiếp, kiểu như vậy. Trong hệ n&agrave;y, 1 l&agrave; k&yacute; hiệu cho S(0) v&agrave; 2 l&agrave; k&yacute; hiệu cho S(S(0)).</p>\n<p>&nbsp;</p>\n<p style=\"text-align: left;\">Ph&eacute;p cộng được định nghĩa theo c&aacute;ch quy nạp, nghĩa l&agrave;:</p>\n<p style=\"text-align: left;\">x + 0 = x</p>\n<p style=\"text-align: left;\">x + S(y) = S(x + y)</p>\n<p style=\"text-align: center;\">&nbsp;</p>\n<p style=\"text-align: left;\">Từ đ&oacute; ta c&oacute; thể suy ra:</p>\n<p style=\"text-align: left;\">1 + 1 = 1 + S(0) = S(1 + 0) = S(1)</p>\n<p style=\"text-align: center;\">&nbsp;</p>\n<p style=\"text-align: left;\">Giờ th&igrave; thay 1 với \"dạng đầy đủ\" của n&oacute; l&agrave; S(0) th&igrave; ta sẽ c&oacute;:</p>\n<p style=\"text-align: left; padding-left: 40px;\"><strong>S(0) + S(0) = S(S(0) + 0) = S(S(0))</strong></p>\n<p style=\"text-align: left;\">Đ&oacute; l&agrave; kết quả m&agrave; ta cần chứng minh.</p>\n<p>_____</p>\n<p>&nbsp;</p>\n<p><em>Trong những trường hợp thường ng&agrave;y, ta cần phải nhớ rằng 1, 2, 3, ... chỉ l&agrave; những k&yacute; hiệu. Ch&uacute;ng kh&ocirc;ng c&oacute; &yacute; nghĩa g&igrave; cho đến khi ta định nghĩa cho ch&uacute;ng, v&agrave; khi ta viết số 1 ta thường nghĩ đến t&iacute;nh chất của phần tử đơn vị. Tuy nhi&ecirc;n, như t&ocirc;i ghi ở tr&ecirc;n, việc n&agrave;y thường phụ thuộc v&agrave;o hệ tiền đề - những \"quy tắc cơ bản\" của ch&uacute;ng ta.</em></p>\n<p>&nbsp;</p>\n<p><em>Nếu ta x&eacute;t rằng, thay v&igrave; l&agrave; số tự nhi&ecirc;n, m&agrave; thay v&agrave;o đ&oacute; l&agrave; số nhị ph&acirc;n 0, 1 với ph&eacute;p cộng l&agrave; mod 2, th&igrave; ta sẽ c&oacute; 1 + 1 = 0. Bạn c&oacute; thể sẽ phản b&aacute;c rằng 0 &ne; 2, tuy nhi&ecirc;n trong hệ tiền đề n&agrave;y (hệ tiền đề m&agrave; t&ocirc;i chưa m&ocirc; tả r&otilde; r&agrave;ng cho lắm) ta c&oacute; thể chứng minh được rằng 0 = 2, trong đ&oacute; 2 l&agrave; viết tắc của 1 + 1 v&agrave; 0 l&agrave; một đơn vị cộng.</em></p>\n<p><em>Thật ra, chỉ cần ghi 1 + 1 = 0 ch&iacute;nh l&agrave; bằng chứng của việc đ&oacute;.</em></p>\n<p>&nbsp;</p>\n<p><em>T&ocirc;i muốn thật nhấn mạnh điều n&agrave;y, bởi v&igrave; đ&acirc;y l&agrave; một phần rất quan trọng trong to&aacute;n học. Ch&uacute;ng ta thường sử dụng c&aacute;c k&yacute; hiệu tự nhi&ecirc;n, như l&agrave; số tự nhi&ecirc;n, trước khi ta định nghĩa n&oacute;. Về sau ta định nghĩa n&oacute; sau cho n&oacute; \"hoạt động theo &yacute; ta muốn\" v&agrave; chỉ khi đ&oacute; ta mới c&oacute; một h&igrave;nh thức khu&ocirc;n khổ to&aacute;n học để c&oacute; thể l&agrave;m việc.</em></p>\n<p>&nbsp;</p>\n<p><em>Những hệ tiền đề, những khu&ocirc;n khổ to&aacute;n học, những thứ n&agrave;y thường được đặt \"trong b&oacute;ng tối\" v&agrave; nếu bạn kh&ocirc;ng biết chỗ n&agrave;o để m&agrave; nh&igrave;n th&igrave; sẽ rất kh&oacute; để t&igrave;m ra ch&uacute;ng.</em></p>\n<p>&nbsp;</p>\n<p><em>Đ&acirc;y l&agrave; l&yacute; do tại sao c&acirc;u hỏi \"Tại sao 1 + 1 = 2?\" gần như l&agrave; v&ocirc; nghĩa - v&igrave; bạn kh&ocirc;ng c&oacute; một h&igrave;nh thức khu&ocirc;n khổ to&aacute;n học, v&agrave; sự diễn giải (t&ocirc;i cho l&agrave; ở dạng tự nhi&ecirc;n) lại rất mơ hồ.</em><br /><br /><em>Nhưng c&ugrave;ng với việc n&agrave;y, c&acirc;u hỏi của bạn cũng rất l&agrave; quan trọng khi bắt đầu với to&aacute;n học, n&oacute; gi&uacute;p bạn biết c&oacute; c&aacute;i g&igrave; để chứng minh, v&agrave; chứng minh như thế n&agrave;o. Đương nhi&ecirc;n l&agrave; cả điều n&agrave;y cũng thiếu đi ngữ cảnh bởi v&igrave; ta cũng cần phải định nghĩa trước ti&ecirc;n c&aacute;i g&igrave; l&agrave; một bằng chứng, v&agrave; những kh&aacute;c nữa.</em></p>\n<p>&nbsp;</p>\n<p>Nguồn:&nbsp;<em><a title=\"StackExchange Math\" href=\"https://math.stackexchange.com/questions/95069/how-would-one-be-able-to-prove-mathematically-that-11-2\" target=\"_blank\" rel=\"noopener\">StackExchange Math</a></em></p>",
    date: "1/1/2011",
    rating: 42,
    rated: true,
    user: {
      name: "Lain",
      avatar: "./mock/user2.jpg",
      id: "WIRED",
    },
  },
  {
    content: "<p>Bằng <strong>3</strong> nhe bạn</p>\n<p>Ch&uacute;c bạn học tốt.</p>",
    date: "2/1/2011",
    rating: 12,
    rated: false,
    user: {
      name: "Nguyễn Văn A",
      avatar: "./mock/user3.jpg",
      id: "1",
    },
  },

  {
    content: "<p>2, vậy cũng hỏi.</p>",
    date: "12/3/2021",
    rating: 5,
    rated: false,
    user: {
      name: "Nguyễn Văn B",
      avatar: "./mock/user4.jpg",
      id: "0",
    },
  },

  {
    content: "<p><span style=\"font-size: 24pt;\">n&egrave;: <img style=\"float: right;\" src=\"https://assets-news.housing.com/news/wp-content/uploads/2020/06/16202612/House-number-numerology-Meaning-of-house-number-2-FB-1200x700-compressed.jpg\" alt=\"House number numerology: Meaning of house number 2\" width=\"804\" height=\"469\" /></span></p>",
    date: "12/3/2021",
    rating: 4,
    rated: false,
    user: {
      name: "D Nguyễn Văn",
      avatar: "./mock/user7.jpg",
      id: "0",
    },
  },

  {
    content: "<p>minh2 cung4 ko bik cau nay nua nhe</p>",
    rating: 1,
    rated: false,
    date: "7/1/2011",
    user: {
      name: "x___zChuốiz___x",
      avatar: "./mock/user5.jpg",
      id: "0",
    },
  },


];
