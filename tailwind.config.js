module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#f0f0f0',
        secondary: '#fcfcfc',
        tertiary: '#8a9e97',
        quaternary: {
          normal: '#00949E',
          dark: '#273f5c',
        },
        quinary: {
          normal: '#6ED698',
          dark: '#61c288',
          darker: '#469465'
        },
      },
      scale: {
        '35': '.35'
      },
      borderWidth: {
        '3': '3px',
      },
    },
  },
  corePlugins: {
    preflight: false,
  },
  variants: {
    extend: {
      textColor: ['active'],
      backgroundColor: ['active'],
      borderColor: ['active'],
      borderWidth: ['active'],
    },
  },
  plugins: [],
}